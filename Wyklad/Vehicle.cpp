#include "Vehicle.h"

///////////////////////////////////////////////////
Vehicle::Vehicle(int x, int y) :_x(x), _y(y)
{
	cout << "Vehicle - construktor " << endl;
}

Vehicle::~Vehicle()
{
}
void Vehicle::Move(int dx, int dy){
	_x += dx;
	_y += dy;
}
void Vehicle::MoveTo(int newX, int newY){
	_x = newX;
	_y = newY;
}
void Vehicle::Display() const{
	cout << "wersja czysto wirtualna " << endl;
}

///////////////////////////////////////////////////
LandVehicle::LandVehicle(int x, int y, int seatsNumber) : Vehicle(x, y)
{
	cout << "LandVehicle - construktor " << endl;
	_seatsNumber = seatsNumber;
}

LandVehicle::~LandVehicle()
{
}
///////////////////////////////////////////////////
Car::Car(int x, int y, int seatsNuber, string color) : LandVehicle(x, y, seatsNuber)
{
	cout << "Car - construktor " << endl;
	_color = color;
}

Car::~Car()
{
}

void Car::Display() const{
	cout << "car " << _x << " " << _y<< endl;
}


#include "SailBoat.h"
WaterWehicle::WaterWehicle(int x, int y) : Vehicle(x, y)
{
	cout << "WaterVehicle - construktor " << endl;
}

WaterWehicle::~WaterWehicle()
{
}
///////////////////////////////////////////////////
///////////////////////////////////////////////////

Boat::Boat(int x, int y) : WaterWehicle(x, y)
{
	cout << "Boat - construktor " << endl;
}

Boat::~Boat()
{
}

void Boat::Display()  const
{
	cout << "Boat - ";
	Vehicle::Display();
}
///////////////////////////////////////////////////


SailBoat::SailBoat(int x, int y) : Boat(x, y)
{
	cout << "Sail boat - construktor " << endl;
}


SailBoat::~SailBoat()
{
}
void SailBoat::Display()  const
{
	cout << "Sailboat - ";
	Vehicle::Display();
}

Amphibia::Amphibia(int x, int y, int seatsNumber) : Car(x, y, seatsNumber, "green"), Boat(x, y)
{
	cout << "Amphibia - construktor " << endl;
	Car::_x;
	Boat::_y = y;
}

Amphibia::~Amphibia()
{
}
void Amphibia::Display()  const
{
	cout << "Amphibia - ";
	Car::Display();
}


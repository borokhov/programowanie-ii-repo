#include "Vector.h"


Vector::Vector(int elementsCount) : Matrix(1,elementsCount)
{
}

Vector::Vector(const Vector& other) : Matrix(other)
{
}
Vector& Vector::operator=(const Vector& other)
{	
	Matrix::operator=(other);

	return *this;
}

Vector::~Vector()
{
}

double&  Vector::operator[](int i){
	return this->_M[0][i];
}

double&  Vector::operator()(int i){
	return this->_M[0][i];
}

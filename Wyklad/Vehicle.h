#pragma once

#include <iostream>
#include <string>

using namespace std;

class Vehicle
{
protected:
	int _x, _y; // position
public:
	Vehicle(int x, int y);
	virtual ~Vehicle() = 0;
	virtual void Move(int dx, int dy);
	virtual void MoveTo(int newX, int newY);
	virtual void Display() const = 0;
};

class LandVehicle : public Vehicle
{
protected:
	double _seatsNumber;
public:
	LandVehicle(int x, int y, int seatsNumber);
	virtual ~LandVehicle();
};

class Car : public LandVehicle
{

protected:
	string _color;
public:
	Car(int x, int y, int seatsNuber, string color);
	~Car();
	virtual void Display() const;
};


class WaterWehicle : public	Vehicle
{
public:
	WaterWehicle(int x, int y);
	~WaterWehicle();

private:

};

class Boat : public WaterWehicle
{

public:
	Boat(int x, int y);
	~Boat();
	virtual void Display() const;
private:

};
class SailBoat : public Boat
{
public:
	SailBoat(int x, int y);
	virtual ~SailBoat();
	virtual void Display() const;
};



class Amphibia : public Car, public Boat
{
public:
	Amphibia(int x, int y, int seatsNumber);
	~Amphibia();
	virtual void Display()  const;
};

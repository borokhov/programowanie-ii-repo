#pragma once
#include "Vehicle.h"


class LandVehicle_Virtual : virtual public Vehicle
{
protected:
	double _seatsNumber;
public:
	LandVehicle_Virtual(int x, int y, int seatsNumber);
	virtual ~LandVehicle_Virtual();
};
//////////////////////////////////////////////////////
class WaterWehicle_Virtual :virtual public	Vehicle
{
public:
	WaterWehicle_Virtual(int x, int y);
	~WaterWehicle_Virtual();
};
//////////////////////////////////////////////////////
class Car_Virtual : virtual public LandVehicle_Virtual
{
protected:
	string _color;
public:
	Car_Virtual(int x, int y, int seatsNuber, string color);
	~Car_Virtual();
	virtual void Display() const;
};
///////////////////////////////////////////////////////////
class Boat_Virtual : virtual public WaterWehicle_Virtual
{

public:
	Boat_Virtual(int x, int y);
	~Boat_Virtual();
	virtual void Display() const;
};

//////////////////////////////////////////////////////////////
class Amphibia_Virtual :  virtual public Boat_Virtual, virtual public Car_Virtual
{
 public:
	 Amphibia_Virtual(int x, int y, int seatsNumber);
	 ~Amphibia_Virtual();
	virtual void Display()  const;
};

//////////////////////////////////////////////////////////


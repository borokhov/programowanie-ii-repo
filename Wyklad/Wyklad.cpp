// Wyklad.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;
#include "Matrix.h"
#include "Shape.h"
#include "Vector.h"
#include "Vehicle.h"
#include "SailBoat.h"
#include "VirtualVehiclesExample.h"
#include "ArrayTemplate.h"


void VehiclesExample(){
	Amphibia a1(1, 2, 2);

	Amphibia_Virtual av(1, 2, 2);

	Car* car_pointer = &a1;
	Boat* boat_pointer = &a1;
	LandVehicle *land_vehicle_pointer = &a1;
	WaterWehicle*water_vehicle_pointer = &a1;

	a1.Display();
	a1.Car::Display();
	a1.Boat::Display();

	boat_pointer->Display();
	car_pointer->Display();
	land_vehicle_pointer->Display();
	water_vehicle_pointer->Display();
}
void VirtualVehiclesExample(){
	
	Amphibia_Virtual av1(1, 2, 2);

	Car_Virtual* car_pointer = &av1;
	Boat_Virtual* boat_pointer = &av1;
	LandVehicle_Virtual *land_vehicle_pointer = &av1;
	WaterWehicle_Virtual*water_vehicle_pointer = &av1;

	av1.Display();
	av1.Car_Virtual::Display();
	av1.Boat_Virtual::Display();

	boat_pointer->Display();
	car_pointer->Display();
	land_vehicle_pointer->Display();
	water_vehicle_pointer->Display();
}

void TemplatesExample(){
	ATHArray<double> a1(3);
	SizedArray<double> sa(1, 10);
	SizedArrayWithId<double, string> saId(1, 2, "Tabliczka");

		cout << a1 << endl;
		cout << a1.getCount() << endl;
		cout << a1[2] << endl;

		cout << sa << endl;
		cout << saId << endl;

		cout << saId.getId().c_str() << endl;
}
void FilesExamples(); // deklaracja

int _tmain(int argc, _TCHAR* argv[])
{

	FilesExamples();


	//TemplatesExample();
	

	//VehiclesExample();

	//VirtualVehiclesExample();

	////Vehicle v1(2,2);

	////LandVehicle lv1(2,2,4);

	//Car	c1(2,2,4,"red");

	//Vehicle *vp = &c1;
	//LandVehicle *lvp = &c1;
	//WaterWehicle * wvp;

	//SailBoat sb(1, 3);
	//sb.Vehicle::Display();
	//
	//vp->Display();
	//lvp->Display();
	//c1.Display();
	//
	//vp = &sb;
	//wvp = &sb;
	//
	//vp->Display();
	//wvp->Display();




	/*Matrix m1(2, 3);

	Matrix m2 = m1;
	
	m2(1, 1) = 2;
	m2[0][1] = 1;

	cout << m2 << endl;

	Vector v1( 4);
	v1(3) = 3;
	v1[2] = 2;

	


	cout << "Zabawa z wektorem " << endl;
	cout << v1 << endl;
	
	Matrix * wm = &v1;

	cout << (*wm)(1) << endl;
	
	Vector v2 = v1;

	v2[0] = 11;

	Vector v3(4);
	
	v3 = v2;

	cout << v2 << endl;
	cout << v3 << endl;
*/

	//Shape s1("Shape");
	//cout << "Ksztalty" << endl;
	//cout << s1 << endl;

	//Rectangle r1(2, 4);
	//Circle c1( 4);

	//cout << r1 << endl;
	//cout << c1 << endl;
	//

	//Shape::shapTypeName = "Ksztalt";
	//cout << Shape::GetClassName() << endl;

	//cout << Rectangle::GetClassName() << endl;

	////polimorfizm

	//Shape & referencja = r1;
	//Shape * wskaznik = &c1;

	//// dynamiczne wi�zanie
	//cout << referencja.Shape::Area() << endl;
	//cout << wskaznik->Perimeter() << endl;
	//cout << wskaznik->Shape::Perimeter() << endl;

	//// virtualny destruktor
	//{
	//	Shape * wskaznik = new Circle(2);

	//	delete wskaznik;
	//}
	system("pause");

	
	return 0;
}


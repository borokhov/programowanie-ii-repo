#pragma once
class InterfaceExample
{
public:
	InterfaceExample();
	~InterfaceExample();
};

class GameObject
{
};

class IDisplayEngine
{
public:
	virtual void Display(GameObject o) = 0;
};

class AsciDisplayEngine : public IDisplayEngine
{
	virtual void Display(GameObject o);
};

class  OpenGlDisplayEngine: IDisplayEngine
{
public:

	virtual void Display(GameObject o);
};



void MainDisplayMethod(IDisplayEngine *engine){
	const int nn = 100;
	GameObject gameObjects[nn];

	for (int i = 0; i < nn; i++)
	{
		engine->Display(gameObjects[i]);
	}
}

void main_loop(){
	IDisplayEngine * display_engine =new AsciDisplayEngine(); // new OpenGlDisplayEngine()
	
	MainDisplayMethod(display_engine);

	delete display_engine;

}

#define intefrace class

#define Interface class

#define implements public

#define DeclareInterface(name) __interface actual_##name {

#define DeclareBasedInterface(name, base) __interface actual_##name \
     : public actual_##base {

#define EndInterface(name) };                \
     Interface name : public actual_##name { \
     public:                                 \
        virtual ~name() {}                   \
     };

//Interface ITest{
//
//};

DeclareInterface(ITest)
	void method1();
	int method2();
EndInterface(ITest)

class Test : implements ITest
{
	void method1();
	int method2();
};

class Unity3DDisplayEngine : implements IDisplayEngine
{


};


#define Interface class

#define DeclareInterface(name) Interface name { \
          public: \
          virtual ~name() {}

#define DeclareBasedInterface(name, base) class name : public base { \
          public: \
          virtual ~name() {}

#define EndInterface };

#define implements public

DeclareInterface(IBar)
virtual int GetBarData() const = 0;
virtual void SetBarData(int nData) = 0;
EndInterface
#include "Weapon.h"
DefaultAtackResult::DefaultAtackResult(int atackValue) :_atackValue(atackValue)
{
}
int DefaultAtackResult::getAtackValue() const{ return _atackValue; }
int DefaultAtackResult::getAtackResoponseValue() const{ return 0; }


Weapon::Weapon(int weaponAtackValue, int weaponDefenseValue) :
_weaponAtackValue(weaponAtackValue),
_weaponDefenseValue(weaponDefenseValue)
{
}


Weapon::~Weapon()
{
}

int Weapon::getAtackValue() const{
	return _weaponAtackValue;
}
int Weapon::getDefenceValue() const{
	return _weaponDefenseValue;
}


//////////////////////////////////
IAtackResult* Sword::Atack(GameCharacter * character)
{
	int newHealth = character->GetHealth() +
		character->GetDefense() - _weaponAtackValue;

	character->SetHealth(newHealth);

	DefaultAtackResult*  atackResult = 
		new DefaultAtackResult(_weaponAtackValue);
	return atackResult;
}
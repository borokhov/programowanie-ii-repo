template<class Type>
class OneWayListNode
{
public:
	Type Data;
	OneWayListNode* next;
	OneWayListNode(){ next = NULL; }
};
template<class Type>
class TwoWayListNode{
public:
	Type Data;
	TwoWayListNode* next;
	TwoWayListNode* prev;
};

template<class Type>
class OneWayList{
protected:
	OneWayListNode< Type > * head;
	OneWayListNode< Type > * current;
	OneWayListNode< Type > * tail;
public:
	OneWayList(){
		head = current = tail = NULL;
	}
	OneWayList(const OneWayList<Type> & other){
		head = new OneWayListNode<Type>();
		this->head->Data = other.head->Data;

		current = head;

		OneWayListNode< Type > * otherCurrent = other.head->next;
		OneWayListNode< Type > * newObject;
		while (otherCurrent != NULL){
			newObject = new OneWayListNode<Type>();
			newObject->Data = otherCurrent->Data;
			
			current->next = newObject;
			current = newObject;

			otherCurrent = otherCurrent->next;
		}
		tail = current;
		tail->next = NULL;
	}
	~OneWayList(){
		current = head;
		while (current != NULL)
		{
			current = current->next;
			delete head;
			head = current;
		}
		current = head = tail = NULL;
	}

	void AddHead(const Type &data){
		if (head == NULL)
		{
			head = new OneWayListNode<Type>();
			head->Data = data;
			head->next = NULL;
			tail = head;
			current = head;
		}
		else{
			OneWayListNode< Type > * newObject;
			newObject = new OneWayListNode<Type>();
			newObject->Data = data;
			newObject->next = head;
			head = newObject;
			current = head;
		}
	}

	void AddAtEnd(const Type &data){
		if (tail == NULL)
		{
			AddHead(data);
		}
		else
		{
			OneWayListNode<Type>* newObject;
			newObject = new OneWayListNode<Type>();
			newObject->Data = data;
			tail->next = newObject;
			tail = newObject;
		}
	}

	void AddAfterCurrent(const Type &data){
		if (current == NULL)
		{
			AddHead(data);
		}
		else
		{
			OneWayListNode<Type>* newObject;
			newObject = new OneWayListNode<Type>();
			newObject->Data = data;
			newObject->next = current->next;

			current->next = newObject;
			current = newObject;
		}
	}

	OneWayListNode< Type > * GetHead() const{
		return head;
	}
};

template <class Type>
ostream &operator<<(ostream& out, const OneWayList<Type> & list)
{
	OneWayListNode<Type> *toOutput = list.GetHead();
	while (toOutput != NULL){
		out << toOutput->Data << endl;
		toOutput = toOutput->next;
	}
	return out;
}


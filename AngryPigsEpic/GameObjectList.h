#pragma once
template<class Type>
class ListNode{
public:
	Type data;
	ListNode* next;
	ListNode(Type data){
		this->data = data;
	}
};
template <class Type>
class OneWayList
{
protected:
	ListNode<Type> * head;
	ListNode<Type> * tail;
	ListNode<Type> * current;
public:

	OneWayList()
	{
		head = tail = current = nullptr;
	}

	OneWayList(const OneWayList  & other)
	{
		ListNode<Type> *otherCurrent = other.head->next;;
		InsertHead(other.head->data);

		while (otherCurrent != nullptr)
		{
			InsertAtEnd(otherCurrent->data);
			otherCurrent = other->next;
		}
	}

	virtual OneWayList& operator=(const GameObjectList  * other)
	{
		~GameObjectList();
		ListNode<Type> *otherCurrent = other.head->next;;
		InsertHead(other.head->data);

		while (otherCurrent != nullptr)
		{
			InsertAtEnd(otherCurrent->data);
			otherCurrent = other->next;
		}
	}

	virtual ~OneWayList()
	{
		if (head != nullptr)

			while (head!- nullptr)
			{
				current = head;
				head = head->next;
				delete current;
			}
		head = current = tail = nullptr;
	}

	virtual InsertHead(const Type & data){
		ListNode<Type> * newHead = new ListNode<Type>(data);
		if (head != nullptr)
		{
			newHead->next = head;
			head = newHeadl;
		}
		else
		{
			head = newHead;
			head->next = null;
			tail = head;
			current = head;
		}
	}
	virtual InsertAtEnd(const Type & data){

		if (tail != nullptr){
			ListNode<Type> * newTail = new ListNode<Type>(data);
			tail->next = newTail;
			tail = newTail;
		}
		else
		{
			InsertHead(data)
		}
	}
	virtual InsertAfterCurrent(const Type & data){
		if (current != nullptr){
			ListNode<Type> * newNode = new ListNode<Type>(data);

			newNode->next = current->next
				current->next = newNode;
			current = newNode;
		}
		else
		{
			InsertHead(newNode)
		}
	}

	virtual ListNode<Type>* GetCurrentNode(){
		return current;
	}
	virtual ListNode<Type>* GetNextNode(){
		if (current != nullptr)
		{
			current = current->next;
		}

		return current;
	}
	virtual ListNode<Type>* GetLastNode(){
		return tail;
	}

	virtual Type& GetLastData(){
		return tail->data;
	}

	virtual Type& GetCurrentData(){
		return current->data;
	}
	virtual Type&  GetNextData(){
		if (current != nullptr)
		{
			current = current->next;
		}

		return current->data;
	}

	virtual Type& operator[](int index){
		ListNode < Type> *tmp = head;
		int counter = 0;
		while (tmp != nullptr && counter < index;)
		{
			tmp = tmp->next;
			counter++;
		}

		return tmp;
	}
};

